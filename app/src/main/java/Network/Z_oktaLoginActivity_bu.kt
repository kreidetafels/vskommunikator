package Network

/**
 * @author: Robert Neumann (Matrikel-Nr.: 15589) on 05.07.2019.
 * @params:
 * ToDo:
 * @note:
 */
// Z_oktaLoginActivity_bu.java

import android.app.PendingIntent
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.example.VSKommunikator.MainActivity
import com.okta.appauth.android.OktaAppAuth
import kotlinx.android.synthetic.main.activity_login.*
import net.openid.appauth.AuthorizationException
import net.openid.appauth.AuthorizationService


class Z_oktaLoginActivity_bu : AppCompatActivity() {

    private var mOktaAuth: OktaAppAuth? = null
    private val mAuthService: AuthorizationService? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mOktaAuth = OktaAppAuth.getInstance(this)

        setContentView(com.example.VSKommunikator.R.layout.activity_login)

        mOktaAuth!!.init(
            this,
            object : OktaAppAuth.OktaAuthListener {
                override fun onSuccess() {
                    auth_button.visibility = View.VISIBLE
                    auth_message.visibility = View.GONE
                    progress_bar.visibility = View.GONE
                }

                override fun onTokenFailure(ex: AuthorizationException) {
                    auth_message.text = ex.toString()
                    progress_bar.visibility = View.GONE
                    auth_button.visibility = View.GONE
                }
            }
        )

        val button = findViewById<Button>(com.example.VSKommunikator.R.id.auth_button)
        button.setOnClickListener { v ->
            val completionIntent = Intent(v.context, MainActivity::class.java)
            val cancIntent = Intent(v.context, Z_oktaLoginActivity_bu::class.java)

            cancIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP

            mOktaAuth!!.login(
                v.context,
                PendingIntent.getActivity(v.context, 0, completionIntent, 0),
                PendingIntent.getActivity(v.context, 0, cancIntent, 0)
            )
        }
    }
}