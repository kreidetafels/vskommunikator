package Network

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.GET



/**
 * @author: Robert Neumann (Matrikel-Nr.: 15589) on 07.07.2019.
 * @params:
 * ToDo:
 * @note:
 */

interface HerokuService {
    @GET("hello")
    fun getData(): Call<ResponseBody>
}