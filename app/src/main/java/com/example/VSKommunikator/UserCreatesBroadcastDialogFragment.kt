package com.example.VSKommunikator

import Interfaces.BroadcastDialogListener
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import com.example.VSKommunikator.Data.BroadcastData
import com.example.VSKommunikator.UITools.GetTimeTool.getCurrentTimestampAsId
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import java.util.*

/**
 * @author: Robert Neumann (Matrikel-Nr.: 15589) on 22.06.2019.
 * @params:
 * @note:
 */

class UserCreatesBroadcastDialogFragment : DialogFragment() {

    private var content: String? = null
    private var documentId: String? = null
    private var playerName: String? = null
    private var playerBroadcastText: String? = null

    val db = FirebaseFirestore.getInstance()
    val broadcastsCollectionRef = db.collection("broadcasts")
    val usersCollectionRef = db.collection("users")
    val currentUser = FirebaseAuth.getInstance()

    private var currentTimeStamp = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Pick a style based on the num.
        val style = STYLE_NO_FRAME
        val theme = R.style.DialogTheme
        setStyle(style, theme)

        //Preparation for updating a player time
        content = arguments?.getString("content")
        documentId = arguments?.getString(
            "broadcastDocumentId"
        )
        playerName = arguments?.getString("playerName")
        playerBroadcastText = arguments?.getString("broadcastDescription")
    }

    // Override the Fragment.onAttach() method to instantiate the
    // NoticeDialogListener
    private lateinit var etBroadcastMessage: EditText


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.layout_dialog_broadcast, container, false)

        val btnCancel = view.findViewById<View>(R.id.buttonCancel) as Button
        val btnAccept = view.findViewById<View>(R.id.buttonAccept) as Button

        etBroadcastMessage = view.findViewById(R.id.etBroadcastMsg)
        val data = HashMap<String, Any>()

        usersCollectionRef
            .get()
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {

                    val currentUserDoc = usersCollectionRef.document(currentUser.uid.toString())
                    currentUserDoc.get().addOnSuccessListener {
                        if (it != null && currentUserDoc.id == currentUser.uid && it.exists()) {
                            playerName = it.get("username").toString()
                        }
                    }
                }
            }

        println("PLAYERNAME: $playerName")
            etBroadcastMessage.setText(playerBroadcastText, TextView.BufferType.EDITABLE)

        btnCancel.setOnClickListener {

            dismiss()
        }

        btnAccept.setOnClickListener {

                playerBroadcastText = etBroadcastMessage.text.toString()
                currentTimeStamp = getCurrentTimestampAsId()

                data["playerName"] = playerName!!
                data["broadcastDocumentId"] = currentTimeStamp!!
                data["playerBroadcastMsg"] = playerBroadcastText!!
                data["playerId"] = currentUser.uid!!

                broadcastsCollectionRef
                    .document(currentTimeStamp)
                    .set(data)
                    .addOnSuccessListener {
                        println("finish")
                    }
                    .addOnFailureListener {
                        println("fail")
                    }


            dismiss()

            val mBroadcastObject = BroadcastData(currentTimeStamp,playerName!!,
                currentUser.uid!!, "")



            val myListener = targetFragment as BroadcastDialogListener
            myListener.onSelectDoneDialog(mBroadcastObject)

        }
        return view

    }

    companion object {
        /**
         * Create a new instance of PlayerWantsToCreateDialogFragment, providing "num" as an
         * argument.
         */
        fun newInstance(): UserCreatesBroadcastDialogFragment {
            val f = UserCreatesBroadcastDialogFragment()
            // Supply num input as an argument.
            val args = Bundle()
            f.arguments = args
            return f
        }
    }

}


