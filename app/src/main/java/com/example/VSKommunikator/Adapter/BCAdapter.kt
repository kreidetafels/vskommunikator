package com.example.VSKommunikator.Adapter

import android.content.ContentValues
import android.content.Context
import android.graphics.BitmapFactory
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.example.VSKommunikator.Data.BroadcastData
import com.example.VSKommunikator.R
import com.example.VSKommunikator.UITools.GlideApp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.lv_broadcast.view.*
import kotlinx.android.synthetic.main.lv_playeroverview.view.*
import org.jetbrains.anko.textColor

/** Created by Robert Neumann on 04.06.2019.
 * Matrikel-Nr.: 15589
 * @param dataSource bla
 *
 */
class BCAdapter(
    context: Context, dataSource: List<BroadcastData>
) : ArrayAdapter<BroadcastData?>(context, 0, dataSource) {
    private val currentUser = FirebaseAuth.getInstance()
    private val usersCollectionRef = FirebaseFirestore.getInstance().collection("users")


    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        // Get view for row item
        val rootView = convertView ?: LayoutInflater.from(this.context).inflate(
            R.layout.lv_broadcast,
            parent,
            false
        )
        val currentContact = getItem(position)

        usersCollectionRef.get()
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {

                    val currentUserDoc =
                        usersCollectionRef.document(currentContact?.playerId.toString())
                    currentUserDoc.get().addOnSuccessListener {
                        if (it != null && it.exists()) {

                            val circularProgressDrawable = CircularProgressDrawable(context)
                            circularProgressDrawable.strokeWidth = 5f
                            circularProgressDrawable.centerRadius = 30f
                            circularProgressDrawable.start()

                            val storageReference = it.get("playerProfilePictureUrl").toString()
                            GlideApp.with(context)
                                .load(storageReference)
                                .placeholder(circularProgressDrawable)
                                .fitCenter()
                                .into(rootView.contactImage)

                            /*                    val bitmapUrl = it.get("playerProfilePictureUrl").toString()
                                                println("vor null: bitmapUrl: " + it.get("playerProfilePictureUrl").toString())
                                                if (bitmapUrl.isNotEmpty() && !bitmapUrl.isNullOrBlank() && bitmapUrl != null) {
                                                    val bitmapURI = Uri.parse(bitmapUrl)
                                                    if (rootView.context != null) {
                                                        val bitmap = MediaStore.Images.Media.getBitmap(
                                                            context?.contentResolver,
                                                            bitmapURI
                                                        )
                                                        rootView.playerOverviewProfilePic.setImageBitmap(bitmap)
                                                    }
                                                }*/
                        }

                    }

                } else {
                    val defaultProfilePic = BitmapFactory.decodeResource(
                        context.resources,
                        com.example.VSKommunikator.R.drawable.ic_portrait_black_24dp
                    )
                    rootView.playerOverviewProfilePic.setImageBitmap(defaultProfilePic)
                    Log.w(ContentValues.TAG, "Error getting documents.", task.exception)
                }
            }
            .addOnFailureListener { exception ->
                Log.d(ContentValues.TAG, "get failed with ", exception)
            }

        rootView.bcHeadline.text = currentContact!!.playerName
        rootView.bcTextview.text = currentContact.description

        rootView.bcHeadline.textColor = R.color.colorAccent
        rootView.bcTextview.textColor = R.color.colorAccent
        return rootView
    }

//    private val inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater


    //1
    /*override fun getCount(): Int {
        return dataSource.size
    }

    //2
    override fun getItem(position: Int): Any {
        return dataSource[position]
    }

    //3
    override fun getItemId(position: Int): Long {
        return position.toLong()
    }
*/
    //4
}