package com.example.VSKommunikator.Adapter

import android.content.ContentValues
import android.content.ContentValues.TAG
import android.content.Context
import android.net.Uri
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import com.example.VSKommunikator.Data.PlayerOverviewData
import com.example.VSKommunikator.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.lv_playeroverview.view.*
import android.graphics.BitmapFactory
import android.graphics.Bitmap
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import com.example.VSKommunikator.UITools.GlideApp
import com.google.firebase.storage.FirebaseStorage




/** Created by Robert Neumann on 04.06.2019.
 * Matrikel-Nr.: 15589
 * @param dataSource bla
 * ToDo: PlayerStatus-Bild updatet nicht immer
 * [x]: playerStatus fixen
 * [x]: Eigene Zeile wird nun potentiell gehighlightet (if Name = Fux)

 *
 */
class PlayerOverviewAdapter(
    context: Context, dataSource: List<PlayerOverviewData>
) : ArrayAdapter<PlayerOverviewData?>(context, 0, dataSource) {

    private var waits: List<PlayerOverviewData> = dataSource
    private val currentUser = FirebaseAuth.getInstance()
    private val usersCollectionRef = FirebaseFirestore.getInstance().collection("users")

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        // Get view for row item
        val rootView = convertView ?: LayoutInflater.from(context).inflate(com.example.VSKommunikator.R.layout.lv_playeroverview, parent, false)
        val currentPlayer = getItem(position)
        var llPlayerOverViewBackground: LinearLayout = rootView.findViewById(com.example.VSKommunikator.R.id.ll_playerOverviewItemBackground)
        var llPlayerOverviewItemBackgroundIconPart: LinearLayout = rootView.findViewById(com.example.VSKommunikator.R.id.ll_playerOverviewItemBackgroundIconPart)

        usersCollectionRef.get()
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {

                    val currentUserDoc = usersCollectionRef.document(currentPlayer?.playerId.toString())
                    currentUserDoc.get().addOnSuccessListener {
                        if (it != null && it.exists()) {

                            val circularProgressDrawable = CircularProgressDrawable(context)
                            circularProgressDrawable.strokeWidth = 5f
                            circularProgressDrawable.centerRadius = 30f
                            circularProgressDrawable.start()

                            val storageReference = it.get("playerProfilePictureUrl").toString()
                            GlideApp.with(context)
                                .load(storageReference)
                                .placeholder(circularProgressDrawable)
                                .fitCenter()
                                .into(rootView.playerOverviewProfilePic)

                            /*                    val bitmapUrl = it.get("playerProfilePictureUrl").toString()
                                                println("vor null: bitmapUrl: " + it.get("playerProfilePictureUrl").toString())
                                                if (bitmapUrl.isNotEmpty() && !bitmapUrl.isNullOrBlank() && bitmapUrl != null) {
                                                    val bitmapURI = Uri.parse(bitmapUrl)
                                                    if (rootView.context != null) {
                                                        val bitmap = MediaStore.Images.Media.getBitmap(
                                                            context?.contentResolver,
                                                            bitmapURI
                                                        )
                                                        rootView.playerOverviewProfilePic.setImageBitmap(bitmap)
                                                    }
                                                }*/
                        }

                    }

                } else {
                    val defaultProfilePic = BitmapFactory.decodeResource(
                        context.resources,
                        R.drawable.ic_portrait_black_24dp
                    )
                    rootView.playerOverviewProfilePic.setImageBitmap(defaultProfilePic)
                    Log.w(TAG, "Error getting documents.", task.exception)
                }
            }
            .addOnFailureListener { exception ->
                Log.d(TAG, "get failed with ", exception)
            }

        when (currentPlayer!!.playerStatus) {
            0 -> rootView.playerStatusImageView.setImageResource(R.drawable.ic_open_alarm)
            -1 -> rootView.playerStatusImageView.setImageResource(R.drawable.ic_players_no_time)
            1 -> rootView.playerStatusImageView.setImageResource(R.drawable.ic_check_circle_black_24dp)
        }

        rootView.playerOverviewName.text = currentPlayer.playerName
        rootView.playerOverviewDate.text = currentPlayer.playerDate
        rootView.playerOverviewFrom.text = currentPlayer.playerTimeFrom
        rootView.playerOverviewTill.text = currentPlayer.playerTimeTill
        rootView.playerOverviewFromPause.text = currentPlayer.playerTimeFromPause
        rootView.playerOverviewTillPause.text = currentPlayer.playerTimeTillPause
        rootView.tvAdditionalText.text = currentPlayer.playerAdditionalText

        if(getItem(position)?.playerId == currentUser.uid){
            llPlayerOverViewBackground.setBackgroundColor(ContextCompat.getColor(context,
                R.color.colorPlayerOverviewOwnItemHighlight))
            llPlayerOverviewItemBackgroundIconPart.setBackgroundColor(ContextCompat.getColor(context,
                R.color.colorPlayerOverviewOwnItemHighlight))
        }
        else{
            llPlayerOverViewBackground.setBackgroundColor(ContextCompat.getColor(context,
                R.color.colorPlayerOverviewItem))
            llPlayerOverviewItemBackgroundIconPart.setBackgroundColor(ContextCompat.getColor(context,
                R.color.colorPlayerOverviewItem))
        }

        return rootView
    }


    override fun notifyDataSetChanged() {
        super.notifyDataSetChanged()

    }



    fun updateList(list : List<PlayerOverviewData>){
        waits = list
    }

    override fun getItem(position: Int): PlayerOverviewData? {
        return super.getItem(position)
    }



    override fun remove(`object`: PlayerOverviewData?) {
        super.remove(`object`)
    }

    //    private val inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    //1
    /*override fun getCount(): Int {
        return dataSource.size
    }

    //2
    override fun getItem(position: Int): Any {
        return dataSource[position]
    }

    //3
    override fun getItemId(position: Int): Long {
        return position.toLong()
    }
*/
    //4
}