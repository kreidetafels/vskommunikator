package com.example.VSKommunikator

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import androidx.fragment.app.Fragment
import java.util.*

/* Created by Robert Neumann on 20.05.2019.
 * Matrikel-Nr.: 15589
 * ToDo:
 */

class DecisionMaker : Fragment() {

    private lateinit var rootView : View
    private lateinit var diceImage : ImageView


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        rootView = inflater.inflate(R.layout.fragment_webview_decisionmaker_overview, container, false)

        val rollButton : Button = rootView.findViewById(R.id.btn_sendrps)
        rollButton.setOnClickListener {
            rollDice()
        }

        diceImage = rootView.findViewById(R.id.dice_image)
        return rootView
    }

    private fun rollDice() {
        val randomInt = Random().nextInt(3)+1
        val drawableResource = when(randomInt){
            1 ->R.drawable.decisionhelper_paper
            2 ->R.drawable.decisionhelper_rock
            else ->R.drawable.decisionhelper_scissor
        }
        diceImage.setImageResource(drawableResource)
        println(randomInt)
    }
}