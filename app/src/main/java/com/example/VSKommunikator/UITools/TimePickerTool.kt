package com.example.VSKommunikator.UITools

import android.app.DatePickerDialog
import android.widget.DatePicker
import android.widget.EditText
import android.app.DatePickerDialog.OnDateSetListener
import android.content.Context
import android.icu.text.SimpleDateFormat
import android.view.View
import android.view.View.OnFocusChangeListener
import java.util.*


/**
 * @author: Robert Neumann (Matrikel-Nr.: 15589) on 04.06.2019.
 * @params:
 * ToDo:
 * Notes:
 */

class TimePickerTool(private val editText: EditText, ctx: Context) : OnFocusChangeListener, OnDateSetListener {
    private val myCalendar: Calendar
    private val ctx = ctx
    init {
        this.editText.onFocusChangeListener = this
        myCalendar = Calendar.getInstance()
    }

    override fun onDateSet(view: DatePicker, year: Int, monthOfYear: Int, dayOfMonth: Int) {
        // this.editText.setText();

        val myFormat = "MMM dd, yyyy" //In which you need put here
        val sdformat = SimpleDateFormat(myFormat, Locale.US)
        myCalendar.set(Calendar.YEAR, year)
        myCalendar.set(Calendar.MONTH, monthOfYear)
        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)

        editText.setText(sdformat.format(myCalendar.getTime()))

    }

    override fun onFocusChange(v: View, hasFocus: Boolean) {
        // TODO Auto-generated method stub
        if (hasFocus) {
            DatePickerDialog(
                ctx, this, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)
            ).show()
        }
    }

}