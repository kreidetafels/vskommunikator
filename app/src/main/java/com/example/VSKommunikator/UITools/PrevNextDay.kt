package com.example.VSKommunikator.UITools

import java.text.SimpleDateFormat
import java.util.*


/* Created by Robert Neumann.
 * Matrikel-Nr.: 15589
 * */



    fun datePlusOneDay(myDateToConvert : String): String? { // Get current date
        var dt2 = myDateToConvert // Start date
        val sdf = SimpleDateFormat("dd-MM-yyyy")
        val c = Calendar.getInstance()
        c.time = sdf.parse(dt2)
        c.add(Calendar.DATE, 1) // number of days to add
        dt2 = sdf.format(c.time) // dt is now the new date
        println("dt2: ${dt2}")
        return dt2
    }


fun dateMinusOneDay(myDateToConvert : String): String? { // Get current date
    var dt2 = myDateToConvert // Start date
    val sdf = SimpleDateFormat("dd-MM-yyyy")
    val c = Calendar.getInstance()
    c.time = sdf.parse(dt2)
    c.add(Calendar.DATE, -1) // number of days to subtract
    dt2 = sdf.format(c.time) // dt is now the new date
    println("dt2: ${dt2}")
    return dt2
}