package com.example.VSKommunikator.UITools

import android.app.TimePickerDialog
import android.content.Context
import android.os.Build
import android.view.View
import android.widget.TextView
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*

/**
 * @author: Robert Neumann (Matrikel-Nr.: 15589) on 24.06.2019.
 * @params:
 * ToDo:
 * @note:
 */

object GetTimeTool{

    fun getTime(textView: TextView, context: Context) {
        val cal = Calendar.getInstance()

        val timeSetListener = TimePickerDialog.OnTimeSetListener { timePicker, hour, minute ->
            cal.set(Calendar.HOUR_OF_DAY, hour)
            cal.set(Calendar.MINUTE, minute)

            textView.text = SimpleDateFormat("HH:mm").format(cal.time)
        }

        textView.setSafeOnClickListener {
            TimePickerDialog(
                context,
                timeSetListener,
                cal.get(Calendar.HOUR_OF_DAY),
                cal.get(Calendar.MINUTE),
                true
            ).show()
        }

    }

    private fun View.setSafeOnClickListener(onSafeClick: (View) -> Unit) {

        val safeClickListener = SafeClickListener {
            onSafeClick(it)
        }
        setOnClickListener(safeClickListener)
    }

    fun getPickedDate(pickedDateStr : String): String{

        val myFormat = "dd-MM-yyyy" // mention the format you need
        val sdf = SimpleDateFormat(myFormat, Locale.GERMANY)

        println("hier ist das datum: $pickedDateStr")
        return sdf.format(this)
    }

    fun getCurrentDate(): String {
        var answer: String = ""
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val current = LocalDateTime.now()
            val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
            answer =  current.format(formatter)
        } else {
            var date = Date();
            val formatter = SimpleDateFormat("yyyy-MM-dd")
            answer = formatter.format(date)
        }
        return answer
    }

    fun getCurrentDateFormatted(datePar:String): String{
        var answer: String = datePar
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val current = LocalDateTime.now()
            val formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy")
            answer =  current.format(formatter)
        } else {
            var date = Date();
            val formatter = SimpleDateFormat("dd-MM-yyyy")
            answer = formatter.format(date)
        }
        return answer
    }


    fun getCurrentDateTime(): String {
        var answer: String = ""
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val current = LocalDateTime.now()
            val formatter = DateTimeFormatter.ofPattern("HH:mm")
            answer =  current.format(formatter)
        } else {
            var date = Date();
            val formatter = SimpleDateFormat("HH:mm")
            answer = formatter.format(date)
        }
        return answer
    }

    fun getCurrentTimestampAsId() : String{
        var documentNameFormatted = ""

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val current = LocalDateTime.now()
            val formatterDocumentName = DateTimeFormatter.ofPattern("yyyy-MM-dd-HH:mm:ss.SSS")
            documentNameFormatted =  current.format(formatterDocumentName)
        } else {
            var date = Date();
            val formatterDocumentName = SimpleDateFormat("yyyy-MM-dd-HH:mm:ss.SSS")
            val formatterDate = SimpleDateFormat("yyyy-MM-dd")
            documentNameFormatted = formatterDocumentName.format(date)
        }
        return documentNameFormatted
    }
}