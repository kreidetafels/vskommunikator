package com.example.VSKommunikator.UITools

import android.view.View

/**
 * @author: Robert Neumann (Matrikel-Nr.: 15589) on 24.06.2019.
 * @params:
 * ToDo:
 * @note: Nimmt eine variable Anzahl an Views an, um die Sichtbarkeit ein- bzw. auszuschalten
 */

object ToggleVisibility {
    fun toggleVisibility(vararg view: View) {
        for (item in view) {
            item.visibility = if (item.visibility == View.INVISIBLE) {
                View.VISIBLE
            } else {
                View.INVISIBLE
            }
        }
    }
}