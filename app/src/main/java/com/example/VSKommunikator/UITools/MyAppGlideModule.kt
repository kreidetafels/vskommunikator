package com.example.VSKommunikator.UITools

/**
 * @author: Robert Neumann (Matrikel-Nr.: 15589) on 01.09.2019.
 * @params:
 * ToDo:
 * @note:
 */
import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

@GlideModule
class MyAppGlideModule : AppGlideModule()