package com.example.VSKommunikator.UITools

import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction

/**
 * @author: Robert Neumann (Matrikel-Nr.: 15589) on 28.06.2019.
 * @params:
 * ToDo:
 * @note:
 */

inline fun FragmentManager.inTransaction(func: FragmentTransaction.() -> Unit) {
    val fragmentTransaction = beginTransaction()
    fragmentTransaction.func()
    fragmentTransaction.commit()
}

