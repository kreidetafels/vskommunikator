package com.example.VSKommunikator.UITools

import android.content.Context
import android.widget.Toast

/**
 * @author: Robert Neumann (Matrikel-Nr.: 15589) on 24.06.2019.
 * @params:
 * ToDo:
 * @note:
 */
object ToastGenerator {
    fun message(context: Context, str: String) {
        Toast.makeText(context, str, Toast.LENGTH_SHORT).show()
    }
}