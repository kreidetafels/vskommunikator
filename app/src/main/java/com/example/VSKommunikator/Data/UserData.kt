package com.example.VSKommunikator.Data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/* Created by Robert Neumann on 04.06.2019.
 * Matrikel-Nr.: 15589
 * Custom Class for User data
 */

@Parcelize
data class UserData (
    val userID: String = "ID",
    var username: String ="Spielername",
    var playerProfilePictureUrl: String =""
) : Parcelable
