package com.example.VSKommunikator.Data

import org.jetbrains.exposed.sql.Database

/**
 * @author: Robert Neumann (Matrikel-Nr.: 15589) on 04.07.2019.
 * @params:
 * ToDo:
 * @note:
 */

object DbSettings{
    val db by lazy{
        Database.connect("jdbc:h2:mem:test", "org.h2.Driver")
    }
}