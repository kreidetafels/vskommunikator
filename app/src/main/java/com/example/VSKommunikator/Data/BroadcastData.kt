package com.example.VSKommunikator.Data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/* Created by Robert Neumann on 04.06.2019.
 * Matrikel-Nr.: 15589
 * Custom Class for BCAdapter data
 */
@Parcelize
data class BroadcastData (
    val broadcastID: String = "13",
    val playerName: String = "Spielername",
    val playerId: String = "1234",
    val description: String = "gasfgasfg"
): Parcelable