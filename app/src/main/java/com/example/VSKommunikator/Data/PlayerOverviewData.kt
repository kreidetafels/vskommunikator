package com.example.VSKommunikator.Data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/* Created by Robert Neumann on 04.06.2019.
 * Matrikel-Nr.: 15589
 * Custom Class for PlayerOverview data
 * ToDo: Overthink which values are needed for the player
 */

@Parcelize
data class PlayerOverviewData (
    val playerId: String = "",
    val documentId: String ="",
    val playerStatus: Int = 0,
    var playerName: String ="Spielername",
    val playerTimeFrom: String="",
    val playerTimeTill: String="",
    val playerTimeFromPause: String="",
    val playerTimeTillPause: String="",
    val playerAdditionalText: String = "",
    val playerDate: String = ""
) : Parcelable
