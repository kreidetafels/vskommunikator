package com.example.VSKommunikator

import Interfaces.PlayerDialogListener
import android.app.DatePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.DialogFragment
import com.example.VSKommunikator.Data.PlayerOverviewData
import com.example.VSKommunikator.UITools.GetTimeTool
import com.example.VSKommunikator.UITools.GetTimeTool.getCurrentTimestampAsId
import com.example.VSKommunikator.UITools.GetTimeTool.getTime
import com.example.VSKommunikator.UITools.ToggleVisibility
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import java.text.SimpleDateFormat
import java.util.*


/**
 * @author: Robert Neumann (Matrikel-Nr.: 15589) on 22.06.2019.
 * @params:
 * [x]: Listener bauen, damit von DialogFragment Daten an das ausgehende Fragment zurückkommen
 * [x]: Datumsauswahl ermöglichen

 * @note:
 */

class PlayerWantsToPlayDialogFragment : DialogFragment() {

    private var content: String? = null
    private var documentId: String? = null
    private var playerName: String? = "Playername"
    private var playerStatus: Int? = null
    private var playerTimeFrom: String? = null
    private var playerTimeFromPause: String? = null
    private var playerTimeTill: String? = null
    private var playerTimeTillPause: String? = null
    private var playerAdditionalText: String? = null


    //Firebase
    val db = FirebaseFirestore.getInstance()
    val playersCollectionRef = db.collection("players")
    val usersCollectionRef = db.collection("users")
    val currentUser = FirebaseAuth.getInstance()


    private var isEditedTime: Boolean = false
    private var currentTimeStamp = ""
    private var currentDateStamp = GetTimeTool.getCurrentDateFormatted(GetTimeTool.getCurrentDate())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Pick a style based on the num.
        val style = STYLE_NO_FRAME
        val theme = R.style.DialogTheme
        setStyle(style, theme)

        //Preparation for updating a player time
        content = arguments?.getString("content")
        documentId = arguments?.getString(
            "documentId"
        )
        playerStatus = arguments?.getInt("playerStatus")
        playerTimeFrom = arguments?.getString("playerTimeFrom")
        playerTimeFromPause = arguments?.getString("playerTimeFromPause")
        playerTimeTill = arguments?.getString("playerTimeTill")
        playerTimeTillPause = arguments?.getString("playerTimeTillPause")
        playerAdditionalText = arguments?.getString("playerAdditionalText")
        isEditedTime = !documentId.isNullOrEmpty()
    }

    // Override the Fragment.onAttach() method to instantiate the
    // NoticeDialogListener
    private lateinit var cbWithAPause: CheckBox
    private lateinit var cbWantsToPlay: CheckBox
    private lateinit var etGameTimeFrom: EditText
    private lateinit var etGameTimeTill: EditText
    private lateinit var tvFromPause: TextView
    private lateinit var etGameTimeFromPause: EditText
    private lateinit var tvTillPause: TextView
    private lateinit var etGameTimeTillPause: EditText
    private lateinit var etAdditionalInfo: EditText
    private var calendarEntry = Calendar.getInstance()
    private lateinit var tv_datepicker: TextView

    private lateinit var btnUpdatestatus: Button

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.layout_dialog, container, false)

        val btnCancel = view.findViewById<View>(R.id.buttonCancel) as Button
        val btnAccept = view.findViewById<View>(R.id.buttonAccept) as Button

        cbWantsToPlay = view.findViewById(R.id.cb_want_to_play)
        tvFromPause = view.findViewById(R.id.tvFromPause)
        tvTillPause = view.findViewById(R.id.tvTillPause)
        etGameTimeFromPause = view.findViewById(R.id.et_gametime_from_pause)
        etGameTimeTillPause = view.findViewById(R.id.et_gametime_till_pause)
        etGameTimeFrom = view.findViewById(R.id.et_gametime_from)
        etGameTimeTill = view.findViewById(R.id.et_gametime_till)
        cbWithAPause = view.findViewById(R.id.cb_with_a_pause)
        etAdditionalInfo = view.findViewById(R.id.etAdditionalText)
        tv_datepicker = view.findViewById(R.id.tv_datepicker)

        etGameTimeFrom.setText(GetTimeTool.getCurrentDateTime(), TextView.BufferType.EDITABLE)
        etGameTimeTill.setText(GetTimeTool.getCurrentDateTime(), TextView.BufferType.EDITABLE)


        val dateSetListener = object : DatePickerDialog.OnDateSetListener {
            override fun onDateSet(
                view: DatePicker, year: Int, monthOfYear: Int,
                dayOfMonth: Int
            ) {
                calendarEntry.set(Calendar.YEAR, year)
                calendarEntry.set(Calendar.MONTH, monthOfYear)
                calendarEntry.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                updateDateInView()
            }
        }


        tv_datepicker.text = GetTimeTool.getCurrentDateFormatted(GetTimeTool.getCurrentDate())

        tv_datepicker.setOnClickListener {
            DatePickerDialog(
                view.context,
                dateSetListener,
                calendarEntry.get(Calendar.YEAR),
                calendarEntry.get(Calendar.MONTH),
                calendarEntry.get(Calendar.DAY_OF_MONTH)
            ).show()
        }


        val data = HashMap<String, Any>()

        usersCollectionRef
            .get()
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {

                    val currentUserDoc = usersCollectionRef.document(currentUser.uid.toString())
                    currentUserDoc.get().addOnSuccessListener {
                        if (it != null && currentUserDoc.id == currentUser.uid && it.exists()) {
                            playerName = it.get("username").toString()
                        }
                    }
                }
            }

        cbWantsToPlay.setOnCheckedChangeListener { _, _ ->
            ToggleVisibility.toggleVisibility(cbWithAPause)
        }

        cbWithAPause.setOnCheckedChangeListener { _, _ ->
            ToggleVisibility.toggleVisibility(
                tvFromPause,
                etGameTimeFromPause,
                etGameTimeTillPause,
                tvTillPause
            )
            etGameTimeFromPause.setText(
                GetTimeTool.getCurrentDateTime(),
                TextView.BufferType.EDITABLE
            )
            etGameTimeTillPause.setText(
                GetTimeTool.getCurrentDateTime(),
                TextView.BufferType.EDITABLE
            )
        }

        getTime(etGameTimeFrom, context!!)
        getTime(etGameTimeTill, context!!)
        getTime(etGameTimeFromPause, context!!)
        getTime(etGameTimeTillPause, context!!)

        if (!isEditedTime) {

        } else if (isEditedTime) {
            etGameTimeFrom.setText(playerTimeFrom, TextView.BufferType.EDITABLE)
            etGameTimeFromPause.setText(playerTimeFromPause, TextView.BufferType.EDITABLE)
            etGameTimeTill.setText(playerTimeTill, TextView.BufferType.EDITABLE)
            etGameTimeTillPause.setText(playerTimeTillPause, TextView.BufferType.EDITABLE)
            etAdditionalInfo.setText(playerAdditionalText, TextView.BufferType.EDITABLE)

            if (playerStatus!! == 0) cbWantsToPlay.isChecked = false
            else if (playerStatus!! == 1) cbWantsToPlay.isChecked = true
        }





        btnCancel.setOnClickListener {

            dismiss()
        }



        btnAccept.setOnClickListener {

            /*ToDo: Funktion auslagern, ist sonst hässlich*/
            if (isEditedTime) {

                if (cbWantsToPlay.isChecked) {
                    playerStatus = 1
                } else if (!cbWantsToPlay.isChecked) {
                    playerStatus = -1
                }


                playerTimeFrom = etGameTimeFrom.text.toString()
                playerTimeTill = etGameTimeTill.text.toString()
                playerTimeFromPause = etGameTimeFromPause.text.toString()
                playerTimeTillPause = etGameTimeTillPause.text.toString()
                playerAdditionalText = etAdditionalInfo.text.toString()
                data["playerName"] = playerName!!
                data["playerStatus"] = playerStatus!!
                data["playerTimeFrom"] = playerTimeFrom!!
                data["playerTimeFromPause"] = playerTimeFromPause!!
                data["playerTimeTill"] = playerTimeTill!!
                data["playerTimeTillPause"] = playerTimeTillPause!!
                data["playerAdditionalText"] = playerAdditionalText!!
                data["playerDate"] = tv_datepicker.text.toString()
                data["playerId"] = currentUser.uid!!
                data["documentId"] = documentId!!
               // currentTimeStamp = documentId!!



                /*#
                ###
                  playersCollectionRef.document(pickedDateStamp).collection("players")
                    .document(currentTimeStamp)
                    .collection("playerEntry")
                    .document(currentTimeStamp).set(data).addOnSuccessListener {
                        println("finish123: " + pickedDateStamp)
                    }
                    .addOnFailureListener {
                        println("fail")
                    }
                ###
                *
                *      db.collection("players")
                        .document(tvCurrentDate.text.toString())
                        .collection("playerEntry")
                * */


                var pickedDateStamp = GetTimeTool.getCurrentDateFormatted(tv_datepicker.text.toString())
                var mydocid = documentId!!




                playersCollectionRef.document(pickedDateStamp).collection("playerEntry")
                    .document(mydocid).set(data).addOnSuccessListener {
                        println("finish123: " + pickedDateStamp)
                    }
                    .addOnFailureListener {
                        println("fail")
                    }



            } else if (!isEditedTime) {

                playerStatus = 0
                playerTimeFrom = etGameTimeFrom.text.toString()
                playerTimeTill = etGameTimeTill.text.toString()
                playerTimeFromPause = etGameTimeFromPause.text.toString()
                playerTimeTillPause = etGameTimeTillPause.text.toString()
                playerAdditionalText = etAdditionalInfo.text.toString()
                println("cbWantsToPlay.isChecked: ${cbWantsToPlay.isChecked}")

                if (cbWantsToPlay.isChecked) {
                    playerStatus = 1
                } else if (!cbWantsToPlay.isChecked) {
                    playerStatus = -1
                }
                println("cbWantsToPlay.isChecked: ${cbWantsToPlay.isChecked}")

                currentTimeStamp = getCurrentTimestampAsId()

                data["playerName"] = playerName!!
                data["playerStatus"] = playerStatus!!
                data["playerTimeFrom"] = playerTimeFrom!!
                data["playerTimeFromPause"] = playerTimeFromPause!!
                data["playerTimeTill"] = playerTimeTill!!
                data["playerTimeTillPause"] = playerTimeTillPause!!
                data["playerAdditionalText"] = playerAdditionalText!!
                data["playerId"] = currentUser.uid!!
                data["documentId"] = currentTimeStamp
                data["playerDate"] = tv_datepicker.text.toString()


                var pickedDateStamp = tv_datepicker.text.toString()


                playersCollectionRef.document(pickedDateStamp).collection("playerEntry")
                    .document(currentTimeStamp).set(data).addOnSuccessListener {
                    println("finish123" + pickedDateStamp)
                }
                    .addOnFailureListener {
                        println("fail")
                    }

            }

            dismiss()

            val mPlayerObject = PlayerOverviewData(
                currentUser.uid!!,
                currentTimeStamp,
                playerStatus!!,
                playerName!!,
                playerTimeFrom!!,
                playerTimeTill!!,
                playerTimeFromPause!!,
                playerTimeTillPause!!,
                playerAdditionalText!!
            )


            val myListener = targetFragment as PlayerDialogListener
            myListener.onSelectDoneDialog(mPlayerObject)

        }
        return view

    }

    private fun updateDateInView() {
        val myFormat = "dd-MM-yyyy" // mention the format you need
        val sdf = SimpleDateFormat(myFormat, Locale.GERMANY)
        tv_datepicker!!.text = sdf.format(calendarEntry.time)
    }

    companion object {
        /**
         * Create a new instance of PlayerWantsToPlayDialogFragment, providing "num" as an
         * argument.
         */
        fun newInstance(): PlayerWantsToPlayDialogFragment {
            val f = PlayerWantsToPlayDialogFragment()

            // Supply num input as an argument.
            val args = Bundle()
            f.arguments = args
            return f
        }
    }

}


