package com.example.VSKommunikator

//import org.joda.time.LocalDate
import Interfaces.PlayerDialogListener
import android.content.ContentValues
import android.content.ContentValues.TAG
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.AdapterView
import android.widget.Button
import android.widget.ListView
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.example.VSKommunikator.Adapter.PlayerOverviewAdapter
import com.example.VSKommunikator.Data.PlayerOverviewData
import com.example.VSKommunikator.UITools.*
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ListenerRegistration
import com.jakewharton.threetenabp.AndroidThreeTen
import kotlinx.android.synthetic.main.fragment_webview_player_overview.*


/* Created by Robert Neumann on 20.05.2019.
 * Matrikel-Nr.: 15589
 * ToDo: - PlayerView --> Custom Adapter --> Expandable machen für bspw. Pausen oder weitere Infos
 *
 * [x] ToDo: - ListItem click --> edit Spielzeit
 * [x] ToDo: - ListItem long --> Lösche Spielzeit
 *
 *
 * [x] ToDo: - Wenn Eintrag von einem selbst, dann Highlighte den Eintrag
 *          --> if(localPlayerName.equals(playerName).rowhighlight
 *
 * [x] ToDo: - Datumauswahl daily
 *
 * [ ] ToDo: - Datumauswahl mittels Datepicker
 *
 * [ ] ToDo: Bug: beim Editieren von eigenem Eintrag wird er doppelt angezeigt (Liste wird nicht richtig aktualisiert, sonst okay)
 *  [ ] ToDo: Bei Tageschange auch automatisch das Datum im PlayerWantsToPlayDialog-Datepicker anpassen
 * * [ ] ToDo: Manchmal wird das falsche Pic angezeigt
 * [ ] ToDo: Nach edit und hinzufügen spinnt die ansicht manchmal
 * [x] ToDo: https://codeburst.io/android-swipe-menu-with-recyclerview-8f28a235ff28  sehr nice art elemente zu editieren (wie in gmail)
 * ToDo: Datenbankzugriff --> Singleton
 * https://blog.mindorks.com/using-snaphelper-in-recyclerview-fc616b6833e8
 * Idea: swipe left / right for more actions
 */
class PlayerOverview : Fragment(), PlayerDialogListener {

    //Lateinit allows initizalizing a non-null property outside a constructor
//    private lateinit var cbWantToPlay: CheckBox
//    private lateinit var cbWithAPause: CheckBox
    private lateinit var rootView: View
//    private lateinit var etGameTimeFrom: EditText
//    private lateinit var etGameTimeTill: EditText
//    private lateinit var tvFromPause: TextView
//    private lateinit var etGameTimeFromPause: EditText
//    private lateinit var tvTillPause: TextView
//    private lateinit var etGameTimeTillPause: fText
    private lateinit var btnPrevDay: Button
    private lateinit var btnNextDay: Button

    //Players CheckedTV
//    private lateinit var checkedTVPlayers: CheckedTextView

    //Floating Action Button
    private lateinit var fab_want_to_play: FloatingActionButton
    private lateinit var fab1: FloatingActionButton
    private lateinit var fabPause: FloatingActionButton
    private lateinit var fabOpen: Animation
    private lateinit var fabClose: Animation
    private lateinit var fabRotate: Animation
    private lateinit var fabRotateAntiClockWise: Animation
    private var isOpen = false
    private lateinit var currentPickedDateTV: TextView

    //Player Overview
    private lateinit var lvPlayerOverview: ListView

    private lateinit var playerOverviewAdapter: PlayerOverviewAdapter

    private lateinit var gameTimeFrom: String
    private lateinit var gameTimeTill: String
    private lateinit var gameTimeFromPause: String
    private lateinit var gameTimeTillPause: String

    //Firestore
    private val db by lazy { FirebaseFirestore.getInstance() }
    private val playersCollectionRef = db.collection("players")
   // private val playersCollectionRef = db.collection("players")
    private val usersCollectionRef = db.collection("users")
    val currentUser = FirebaseAuth.getInstance()
    private var playerName: String? = "Playername"
    private var registration: ListenerRegistration? = null

    //ListItem
    var listUsers = mutableListOf<PlayerOverviewData>()
  //  private var dateTime : LocalDate = LocalDate.now()

//    val fab: View = findViewById(R.id.fab)
//    fab.setOnClickListener { view ->
//        Snackbar.make(view, "Here's a Snackbar", Snackbar.LENGTH_LONG)
//            .setAction("Action", null)
//            .show()
//    }


    var value: String? = null


    override fun onStart() {
        super.onStart()
        currentPickedDateTV.text = GetTimeTool.getCurrentDateFormatted(GetTimeTool.getCurrentDate())


//wieder drin
        db.collection("players")
            .document(tvCurrentDate.text.toString())
//            .document(GetTimeTool.getCurrentDateFormatted(currentPickedDateTV.text.toString()))
            .collection("playerEntry")

      //  playersCollectionRef
            .addSnapshotListener { value, e ->
                if (e != null) {
                    Log.w(TAG, "Listen failed.", e)
                    return@addSnapshotListener
                }
                if (!value!!.isEmpty) {

                    listUsers.clear()

                    for (doc in value) {
                        val playerDataObj = doc.toObject(PlayerOverviewData::class.java)
                        listUsers.add(playerDataObj)
                    }
                    playerOverviewAdapter.notifyDataSetChanged()
                    playerOverviewAdapter.notifyDataSetInvalidated()                }
            }
    }


    override fun onStop() {
        super.onStop()
        registration?.remove()
    }

    override fun onPause() {
        super.onPause()
    }

    fun getPlayersFromServer() {

        if (currentPickedDateTV.text != null) {

println("tvCurrentDate ${tvCurrentDate.text.toString()}")

        db.collection("players")
            .document(tvCurrentDate.text.toString())
//            .document(GetTimeTool.getCurrentDateFormatted(currentPickedDateTV.text.toString()))
            .collection("playerEntry")


            // db.collection("players")
            .get()
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    listUsers.clear()


                    var playerDataFromServer = PlayerOverviewData()

                    /*wenn datum = datum in auswahl, dann zeige die jeweiligen tageseinträge an
                    * allerdings muss das auch klappen, wenn keine einträge für den tag vorhanden sind*/



                    for (document in task.result!!) {
                        Log.d(ContentValues.TAG, "dapet" + document.id + " => " + document.data)

                        //   playerDataFromServer.add()
                        listUsers.add(
                            PlayerOverviewData(
                                document.get("playerId").toString(),
                                document.get("documentId").toString(),
                                document.get("playerStatus").toString().toInt(),
                                document.get("playerName").toString(),
                                document.get("playerTimeFrom").toString(),
                                document.get("playerTimeTill").toString(),
                                document.get("playerTimeFromPause").toString(),
                                document.get("playerTimeTillPause").toString(),
                                document.get("playerAdditionalText").toString(),
                                document.get("playerDate").toString()
                            )
                        )


                    }

//                    if (context != null && listUsers != null) {
                    playerOverviewAdapter = PlayerOverviewAdapter(context!!, listUsers)
//                    }
                    lvPlayerOverview.adapter = playerOverviewAdapter

                    println("playerdata: $playerDataFromServer")

                } else {
                    Log.w(ContentValues.TAG, "Error getting documents.", task.exception)
                }
            }
    }

    }

    val childEventListener = object : ChildEventListener {
        override fun onCancelled(p0: DatabaseError) {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        override fun onChildMoved(p0: DataSnapshot, p1: String?) {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        override fun onChildChanged(p0: DataSnapshot, p1: String?) {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        override fun onChildRemoved(p0: DataSnapshot) {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        override fun onChildAdded(dataSnapshot: DataSnapshot, previousChildName: String?) {
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        rootView = inflater.inflate(R.layout.fragment_webview_player_overview, container, false)




        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        AndroidThreeTen.init(context)
        initViews(view)
        getPlayersFromServer()

        createPlayerOverviewList()
    }


    private fun View.setSafeOnClickListener(onSafeClick: (View) -> Unit) {

        val safeClickListener = SafeClickListener {
            onSafeClick(it)
        }
        setOnClickListener(safeClickListener)
    }

    private fun createPlayerOverviewList() {


        playerOverviewAdapter = PlayerOverviewAdapter(context!!, listUsers)
        lvPlayerOverview.adapter = playerOverviewAdapter
        val lvPlayerOverViewOnClickListener = lvPlayerOverview.adapter


        lvPlayerOverview.onItemClickListener =
            AdapterView.OnItemClickListener { parent, view, position, id ->

                var playerFromServer =
                    lvPlayerOverview.getItemAtPosition(position) as PlayerOverviewData

               // view.setSafeOnClickListener{




                if(currentUser.uid!! == playerFromServer.playerId){

                    println("documentId JETZT: ${playerFromServer.documentId} -- Date: ${tvCurrentDate.text.toString()}")

                    db.collection("players")
                        .document(tvCurrentDate.text.toString())
                        .collection("playerEntry").document(playerFromServer.documentId)
                    .get().addOnSuccessListener {

                        createPlayerWantsToPlayDialog(
                            0,
                            playerFromServer.documentId,
                            playerFromServer.playerStatus,
                            playerFromServer.playerId,
                            playerFromServer.playerName,
                            playerFromServer.playerTimeFrom,
                            playerFromServer.playerTimeTill,
                            playerFromServer.playerTimeFromPause,
                            playerFromServer.playerTimeTillPause,
                            playerFromServer.playerAdditionalText,
                            playerFromServer.playerDate
                        )
                    }
                }else{
                    ToastGenerator.message(parent.context, getString(R.string.alert_you_can_only_edit_your_entrys))
                }
                playerOverviewAdapter.notifyDataSetInvalidated()
                playerOverviewAdapter.notifyDataSetChanged()

                }
      //      }




        lvPlayerOverview.setOnItemLongClickListener { parent, view, position, id ->
            var playerFromServer =
                lvPlayerOverview.getItemAtPosition(position) as PlayerOverviewData

            if(currentUser.uid!!.equals(playerFromServer.playerId)) {

                var playerFromServer =
                lvPlayerOverview.getItemAtPosition(position) as PlayerOverviewData




           //     db.collection("players")
            //        .document(tvCurrentDate.text.toString())
//            .document(GetTimeTool.getCurrentDateFormatted(currentPickedDateTV.text.toString()))
             //       .collection("playerEntry")
             //   playersCollectionRef
                db.collection("players")
                    .document(tvCurrentDate.text.toString())
                    .collection("playerEntry").document(playerFromServer.documentId).delete()
                .addOnSuccessListener { Log.d(TAG, "DocumentSnapshot successfully deleted!")
                    playerOverviewAdapter.remove(playerFromServer)
                    playerOverviewAdapter.notifyDataSetChanged()
                    playerOverviewAdapter.notifyDataSetInvalidated()
                }
                .addOnFailureListener { e -> Log.w(TAG, "Error deleting document", e) }
            }
            else{
                ToastGenerator.message(parent.context, getString(R.string.alert_you_can_only_delete_your_entrys))
            }
            true
        }

    }

    fun plusOneDay(theDate:String): String? {
        var returnedDate = datePlusOneDay(theDate)
        return returnedDate
    }

    fun minusOneDay(theDate:String): String? {
        var returnedDate = dateMinusOneDay(theDate)
        return returnedDate
    }


    private fun initViews(view: View) {
        lvPlayerOverview = view.findViewById(R.id.lv_playeroverview)

        currentPickedDateTV = view.findViewById(R.id.tvCurrentDate)

        btnPrevDay = btn_prevDay
        btnNextDay = btn_nextDay


        btnNextDay.setOnClickListener {

            var pickedDate = currentPickedDateTV.text.toString()
            var pickedDatePlusOne = plusOneDay(pickedDate)
            currentPickedDateTV.text = pickedDatePlusOne
            getPlayersFromServer()
        }

        btnPrevDay.setOnClickListener {
            var pickedDate = currentPickedDateTV.text.toString()
            var pickedDateMinusOne = minusOneDay(pickedDate)
            currentPickedDateTV.text = pickedDateMinusOne
            getPlayersFromServer()
        }

        fabOpen = AnimationUtils.loadAnimation(context, R.anim.fab_open)
        fabClose = AnimationUtils.loadAnimation(context, R.anim.fab_close)
        fabRotate = AnimationUtils.loadAnimation(context, R.anim.rotate_clockwise)
        fabRotateAntiClockWise = AnimationUtils.loadAnimation(context, R.anim.rotate_anti_clockwise)

        fab_want_to_play = rootView.findViewById(R.id.fab_want_to_play)
        fab1 = rootView.findViewById(R.id.fab1)
        fabPause = rootView.findViewById(R.id.fabPause)

        fab_want_to_play.setOnClickListener { view ->
            checkId(view.id)
        }

        fabPause.setOnClickListener { view ->
            //            checkId(view.id)
            checkId(view.id)

        }

        fab1.setOnClickListener { view ->
            //            checkId(view.id)
            checkId(view.id)

        }
    }

    private fun checkId(view: Int) {
        usersCollectionRef
            .get()
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val currentUserDoc = usersCollectionRef.document(currentUser.uid.toString())
                    currentUserDoc.get().addOnSuccessListener {
                        if (it != null && currentUserDoc.id == currentUser.uid && it.exists()) {
                            playerName = it.get("username").toString()
                        }
                    }
                }
            }


        when (view) {
            R.id.fab_want_to_play ->
                animateFAB()

            R.id.fab1 -> {
                playerOverviewAdapter.notifyDataSetChanged()
                playerOverviewAdapter.notifyDataSetInvalidated()
                //1 = neu, 0 = edit
                //Möglicher Bug --> playerStatus könnt auf 1 bleiben
                createPlayerWantsToPlayDialog(
                    1,
                    GetTimeTool.getCurrentTimestampAsId(),
                    1,
                    currentUser.uid.toString(),
                    playerName!!,
                    "",
                    "",
                    "",
                    "",
                    "",
                    ""
                )
            }

            R.id.fabPause -> {
                playerOverviewAdapter.notifyDataSetChanged()
                playerOverviewAdapter.notifyDataSetInvalidated()            }

        }
    }

    private fun animateFAB() {
        if (isOpen) {
            fab_want_to_play.startAnimation(fabRotateAntiClockWise)
            fab1.startAnimation(fabClose)
            fabPause.startAnimation(fabClose)
            fab1.isClickable = false
            fabPause.isClickable = false
            isOpen = false

        } else {
            fab_want_to_play.startAnimation(fabRotate)
            fab1.startAnimation(fabOpen)
            fabPause.startAnimation(fabOpen)
            fab1.isClickable = true
            fabPause.isClickable = true
            isOpen = true


        }
    }

    override fun onSelectDoneDialog(
        mPlayer: PlayerOverviewData
    ) {
        when (mPlayer.playerStatus) {
            -1 -> {
                listUsers.add(
                    PlayerOverviewData(
                        mPlayer.playerId,
                        mPlayer.documentId,
                        R.drawable.ic_players_no_time,
                        mPlayer.playerName,
                        mPlayer.playerTimeFrom,
                        mPlayer.playerTimeTill,
                        mPlayer.playerTimeFromPause,
                        mPlayer.playerTimeTillPause,
                        mPlayer.playerAdditionalText,
                        mPlayer.playerDate
                    )
                )
            }

            1 -> {
                listUsers.add(
                    PlayerOverviewData(
                        mPlayer.playerId,
                        mPlayer.documentId,
                        R.drawable.ic_check_circle_black_24dp,
                        mPlayer.playerName,
                        mPlayer.playerTimeFrom,
                        mPlayer.playerTimeTill,
                        mPlayer.playerTimeFromPause,
                        mPlayer.playerTimeTillPause,
                        mPlayer.playerAdditionalText,
                        mPlayer.playerDate
                    )
                )
            }

            0 -> {
                listUsers.add(
                    PlayerOverviewData(
                        mPlayer.playerId,
                        mPlayer.documentId,
                        R.drawable.ic_open_alarm,
                        mPlayer.playerName,
                        mPlayer.playerTimeFrom,
                        mPlayer.playerTimeTill,
                        mPlayer.playerTimeFromPause,
                        mPlayer.playerTimeTillPause,
                        mPlayer.playerAdditionalText,
                        mPlayer.playerDate
                    )
                )
            }
        }
        playerOverviewAdapter.notifyDataSetChanged()
        playerOverviewAdapter.notifyDataSetInvalidated()    }

    override fun onCancelDialog(cancelDialog: Unit) {
        playerOverviewAdapter.notifyDataSetChanged()
        playerOverviewAdapter.notifyDataSetInvalidated()    }

    private fun createPlayerWantsToPlayDialog(
        addOrUpdate: Int,
        playerDocumentId: String,
        playerStatus: Int,
        playerId: String,
        playerName: String,
        playerTimeFrom: String,
        playerTimeTill: String,
        playerTimeFromPause: String,
        playerTimeTillPause: String,
        playerAdditionalText: String,
        playerDate: String
    ) {
        val ft = fragmentManager!!.beginTransaction()
        when (addOrUpdate) {
            1 -> {
                val newFragment = PlayerWantsToPlayDialogFragment.newInstance(
                )
                newFragment.setTargetFragment(this, 0)
                newFragment.show(ft, "dialog")
            }
            0 -> {
                val newFragment = PlayerWantsToPlayDialogFragment.newInstance(
                )
                val args = Bundle()
                args.putSerializable("documentId", playerDocumentId)
                args.putInt("playerStatus", playerStatus)
                args.putSerializable("playerTimeFrom", playerTimeFrom)
                args.putSerializable("playerTimeFromPause", playerTimeFromPause)
                args.putSerializable("playerTimeTill", playerTimeTill)
                args.putSerializable("playerTimeTillPause", playerTimeTillPause)
                args.putSerializable("playerAdditionalText", playerAdditionalText)

                newFragment.arguments = args
                newFragment.setTargetFragment(this, 0)
                newFragment.show(ft, "dialog")
            }
        }

    }
}