package com.example.VSKommunikator

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.SignInButton
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.auth.UserProfileChangeRequest
import kotlinx.android.synthetic.main.activity_main.*


/* Created by Robert Neumann on 04.06.2019.
 * Matrikel-Nr.: 15589
 *
 * Hauptklasse für weitere Routinen
 *
 * ToDo: Outsource "heavy" operations in worker threads (e.g. setting the players
 ** - https://stackoverflow.com/a/51943047
 ** - test it with removing offscreenPageLimit
 ** -
 *
 * ToDo: Remove MainActivitySignin
 * */


class MainActivity : AppCompatActivity() {

    val RC_SIGN_IN: Int = 1

    lateinit var mGoogleSignInClient: GoogleSignInClient
    lateinit var mGoogleSignInOptions: GoogleSignInOptions
//    lateinit var loginButton: SignInButton
    private lateinit var firebaseAuth: FirebaseAuth


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
//        setSupportActionBar(toolbar)

        configureGoogleSignIn()

//        val loginButton = findViewById<SignInButton>(R.id.google_button)

        google_button.setOnClickListener {
            signIn()
        }
        firebaseAuth = FirebaseAuth.getInstance()

        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
            != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                    arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),0
                    )
                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            // Permission has already been granted
        }

    }

    override fun onStart() {
        super.onStart()

        val currentUser = FirebaseAuth.getInstance().currentUser
        if (currentUser == null) {
            Toast.makeText(this, "currentUser == null", Toast.LENGTH_LONG).show()

            // No user is signed in
        } else {

            val profileUpdates = UserProfileChangeRequest.Builder().
                setDisplayName("HorstFux").build()

            currentUser.updateProfile(profileUpdates).addOnCompleteListener {
                if (it.isSuccessful) {
                    Log.d("MainAct", "User profile updated to ${currentUser.displayName}.")
                    Log.d("MainAct", "User updated result: ${it.result}")

                }
            }.addOnFailureListener{
                Log.d("MainAct", "Failed to create user: ${it.message}")
            }



            Toast.makeText(this, "${currentUser.displayName} logged in with id: ${currentUser.uid}", Toast.LENGTH_LONG).show()
            println("${currentUser.displayName} logged in with id")
            startVSKOverview()
        }

    }

    private fun configureGoogleSignIn() {
        mGoogleSignInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()
        mGoogleSignInClient = GoogleSignIn.getClient(this, mGoogleSignInOptions)
    }

    private fun signIn() {
        val signInIntent: Intent = mGoogleSignInClient.signInIntent
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SIGN_IN) {
            val task: Task<GoogleSignInAccount> = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                val account = task.getResult(ApiException::class.java)
                firebaseAuthWithGoogle(account!!)
            } catch (e: ApiException) {
                Toast.makeText(this, "Google sign in failed:(", Toast.LENGTH_LONG).show()
                println("EXCEPTION: " + e)
            }
        }
    }

    private fun firebaseAuthWithGoogle(acct: GoogleSignInAccount) {
        val credential = GoogleAuthProvider.getCredential(acct.idToken, null)
        firebaseAuth.signInWithCredential(credential).addOnCompleteListener {
            if (it.isSuccessful) {
//                startActivity(MainActivity.getLaunchIntent(this))
                startVSKOverview()

            } else {
                Toast.makeText(this, "Google sign in failed:(", Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun startVSKOverview(){



        val vskOverviewIntent = Intent(this, Main2Activity::class.java)
        startActivity(vskOverviewIntent)
//        val adapter = ViewPagerAdapter(supportFragmentManager)
//
//        adapter.addFragment(PlayerOverview(), getString(R.string.playerOverview_title))
//        adapter.addFragment(BroadcastOverview(), getString(R.string.broadcastOverview_title))
//        adapter.addFragment(DecisionMaker(), getString(R.string.decisionMakerOverview_title))
//        adapter.addFragment(SettingsOverview(), getString(R.string.settingsOverview_title))
//        viewPager.offscreenPageLimit = 4
//        viewPager.adapter = adapter
//        tabs.setupWithViewPager(viewPager)
    }

    companion object {
        fun getLaunchIntent(from: Context) = Intent(from, Main2Activity::class.java).apply {
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        }
    }
}

