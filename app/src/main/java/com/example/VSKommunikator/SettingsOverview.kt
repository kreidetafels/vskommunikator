package com.example.VSKommunikator

import android.app.Activity
import android.content.ContentValues
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.provider.MediaStore
import android.util.Log
import android.widget.EditText
import android.widget.ImageView
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.example.VSKommunikator.Data.UserData
import com.example.VSKommunikator.UITools.GlideApp
import com.example.VSKommunikator.UITools.ToastGenerator
import com.google.firebase.auth.FirebaseAuth
import com.firebase.ui.auth.AuthUI
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import java.io.ByteArrayOutputStream
import java.util.*


/* Created by Robert Neumann on 29.05.2019.
 * Matrikel-Nr.: 15589
 * [x] ToDo: Changeable username, profile picture, ...
 *  ToDo: Make username unique
 *  ToDo: Update usernames in players database after changing it in settings. 
 */

class SettingsOverview : Fragment() {

    //    private val BASE_URL = "https://vskommunikator-db.herokuapp.com/"
//    private var service: BookService? = null
    private lateinit var settingsLogout: Button
    private lateinit var selectProfilePicture: Button
    private lateinit var saveProfile: Button
    private lateinit var profilePicture: ImageView
    private lateinit var username: EditText
    var selectedProfilePicture: Uri? = null
    val currentUser = FirebaseAuth.getInstance()

    //Firebase:
    private val usersCollectionRef = FirebaseFirestore.getInstance().collection("users")


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
/*
        val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        service = retrofit.create(BookService::class.java)
        val service = retrofit.create(BookService::class.java)
*/
    }

    override fun onStart() {
        super.onStart()
        usersCollectionRef
            .addSnapshotListener { value, e ->
                if (e != null) {
                    Log.w("Settings", "Listen failed.", e)
                    return@addSnapshotListener
                }
                if (!value!!.isEmpty) {
                    for (doc in value) {
                        if (doc.exists()) {
                            val usersDataObj = doc.toObject(UserData::class.java)
                            Log.d(
                                "Settings",
                                "!!!usersDataObj!!! -- userId: ${usersDataObj.userID} + username: ${usersDataObj.username} Link to ProfilePic: ${usersDataObj.playerProfilePictureUrl}"
                            )
                        }

                    }
                }
            }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_settings, container, false)

        settingsLogout = view.findViewById(R.id.settingsLogout)
        selectProfilePicture = view.findViewById(R.id.selectProfilePicture)
        saveProfile = view.findViewById(R.id.saveProfile)
        profilePicture = view.findViewById(R.id.profilePicture)
        username = view.findViewById(R.id.username)


        val auth = FirebaseAuth.getInstance()
        if (auth.currentUser != null) {
            Log.d("Settings", "onCreateView - Signed In")
        }

        usersCollectionRef
            .get()
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {

                    val currentUserDoc = usersCollectionRef.document(currentUser.uid.toString())
                    currentUserDoc.get().addOnSuccessListener {
                        if (it != null && currentUserDoc.id == currentUser.uid && it.exists()) {
                            username.setText(it.get("username").toString())

                            val circularProgressDrawable = CircularProgressDrawable(view.context)
                            circularProgressDrawable.strokeWidth = 5f
                            circularProgressDrawable.centerRadius = 30f
                            circularProgressDrawable.start()

                            val storageReference = it.get("playerProfilePictureUrl").toString()
                            GlideApp.with(this)
                                .load(storageReference)
                                .placeholder(circularProgressDrawable)
                                .override(400,400)
                                .into(profilePicture)

                        }

                    }

                } else {
                    Log.w(ContentValues.TAG, "Error getting documents.", task.exception)
                }
            }


        settingsLogout.setOnClickListener {
            AuthUI.getInstance().signOut(context!!)
                .addOnCompleteListener {
                    val intent = Intent(activity, MainActivity::class.java)
                    activity?.startActivity(intent)
                }
        }

        selectProfilePicture.setOnClickListener {
            val intent = Intent(Intent.ACTION_PICK)
            intent.type = "image/*"
            startActivityForResult(intent, 0)
        }

        saveProfile.setOnClickListener {
            saveUserToFirebase()
        }
        /*       this listener will be called when there is change in firebase user session
                  FirebaseAuth.AuthStateListener { firebaseAuth ->
                      val user = firebaseAuth.currentUser
                      if (user == null) {
                          ToastGenerator.message(context!!, "Ausgeloggt, bitte App neu starten")
                      }
                  }


          auth.addAuthStateListener{
              if(auth.currentUser == null){
                  activity?.finish()
                  val intent = Intent (activity, Main2Activity::class.java)
                  activity?.startActivity(intent)
              }
          }

          btnLogout.setOnClickListener{


              heroku
              val createCall = service?.all()

              createCall?.enqueue(object : Callback<List<Book>>{
                  override fun onFailure(call: Call<List<Book>>, t: Throwable) {
                  }

                  override fun onResponse(call: Call<List<Book>>, response: Response<List<Book>>) {

                      tvResult.text = "ALL BOOKS by ISBN:\n"
                      for(n in response.body()!!){
                          tvResult.append(n.isbn+"\n")
                      }
                  }

              })
          }

          addBookButton.setOnClickListener {

              Heroku
              val call = service?.create(Book(i,etIsbnInput.text.toString()))
              i++


              call?.enqueue(object : Callback<Book>{
                  override fun onFailure(call: Call<Book>, t: Throwable) {
                      t.printStackTrace()
                      tvResult.text = t.toString()
                  }

                  override fun onResponse(call: Call<Book>, response: Response<Book>) {

                      val newBook = response.body()

                      tvResult.text = "success mit : " + newBook?.isbn
                  }
       /heroku

                  override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                      //handle error here
                      val stringResponse = t.toString()
                      auth_message.text = "fail: " + stringResponse
                  }

                  override fun onResponse(call: Call<ResponseBody>, response: Response<Book>) {
                      //your raw string response
                      val newBook = response.body()
                      tvResult.text = "success: " + newBook?.isbn
                  }
              }*/
        return view
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 0 && resultCode == Activity.RESULT_OK && data != null) {
            Log.d("Settings", "Profile pic selected")
            selectedProfilePicture = data.data
            val bitmap =
                MediaStore.Images.Media.getBitmap(activity?.contentResolver, selectedProfilePicture)
            val baos = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.JPEG, 20, baos)
            val fileInBytes = baos.toByteArray()

            profilePicture.setImageBitmap(bitmap)
            uploadPictureToFirebase(fileInBytes)
        }
    }


    private fun uploadPictureToFirebase(fileInBytes: ByteArray) {


        if (selectProfilePicture == null) return

        val filename = UUID.randomUUID().toString()
        val ref = FirebaseStorage.getInstance().getReference("images/$filename")

        ref.putBytes(fileInBytes!!).addOnSuccessListener {
            Log.d("Settings", "Profile pic uploaded: ${it.metadata?.path}")

            ref.downloadUrl.addOnSuccessListener { itUrl ->
                itUrl.toString()
                Log.d("Settings", "File Location: ${itUrl}")
            }
        }
    }

    private fun saveUserToFirebase() {
        val uid = FirebaseAuth.getInstance().uid
        val ref = FirebaseDatabase.getInstance().getReference("/users/$uid")
        val usernameFromEdittext = username.text.toString()
        val docName = currentUser.uid.toString()


        val data = HashMap<String, Any>()

        data["userID"] = uid!!
        data["username"] = usernameFromEdittext
        data["playerProfilePictureUrl"] = selectedProfilePicture.toString()

        usersCollectionRef
            .document(docName)
            .set(data)
            .addOnSuccessListener {
                println("finish")
                Log.d("Settings", "Saved profile with data: ${data.entries}")
            }
            .addOnFailureListener {
                println("fail")
            }





        if (!uid.isNullOrEmpty() && usernameFromEdittext.isNotBlank() && usernameFromEdittext.isNotEmpty()) {
            val user = UserData(uid, usernameFromEdittext, selectedProfilePicture.toString())
            ref.setValue(user).addOnSuccessListener {
                Log.d(
                    "Settings",
                    "Userid: ${user.userID} Username: ${user.username} Url: ${user.playerProfilePictureUrl}"
                )
            }
        } else {
            ToastGenerator.message(
                context!!,
                "Fehler beim Speichern, Username darf nicht leer sein"
            )
            Log.d(
                "Settings",
                "Fehler beim Speichern: uid: $uid ref: $ref username: $usernameFromEdittext"
            )
        }
    }
}




