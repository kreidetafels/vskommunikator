package com.example.VSKommunikator

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.VSKommunikator.Adapter.ViewPagerAdapter

import kotlinx.android.synthetic.main.activity_main2.*

class Main2Activity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)
        setSupportActionBar(toolbar2)
        supportActionBar?.title = getString(R.string.app_name)


        val adapter = ViewPagerAdapter(supportFragmentManager)
        adapter.addFragment(PlayerOverview(), getString(R.string.playerOverview_title))
        adapter.addFragment(BroadcastOverview(), getString(R.string.broadcastOverview_title))
//        adapter.addFragment(DecisionMaker(), getString(R.string.decisionMakerOverview_title))
        adapter.addFragment(SettingsOverview(), getString(R.string.settingsOverview_title))
        viewPager2.offscreenPageLimit = 3
        viewPager2.adapter = adapter
        tabs2.setupWithViewPager(viewPager2)
    }

}
