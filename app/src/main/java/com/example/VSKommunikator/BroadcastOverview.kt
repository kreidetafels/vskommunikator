package com.example.VSKommunikator

import Interfaces.BroadcastDialogListener
import android.content.ContentValues
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.AdapterView
import android.widget.ListView
import androidx.fragment.app.Fragment
import com.example.VSKommunikator.Adapter.BCAdapter
import com.example.VSKommunikator.Data.BroadcastData
import com.example.VSKommunikator.Data.PlayerOverviewData
import com.example.VSKommunikator.UITools.GetTimeTool
import com.example.VSKommunikator.UITools.ToastGenerator
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ListenerRegistration
import kotlinx.android.synthetic.main.fragment_webview_broadcast_overview.*
import kotlinx.android.synthetic.main.lv_broadcast.*

/* Created by Robert Neumann on 20.05.2019.
 * Matrikel-Nr.: 15589
 * ToDo:
 */
class BroadcastOverview : Fragment(), BroadcastDialogListener {
    override fun onSelectDoneDialog(mPlayerObject: BroadcastData) {
            listBroadcasts.add(
                BroadcastData(
                    mPlayerObject.broadcastID,
                    mPlayerObject.playerId,
                    mPlayerObject.playerName,
                    mPlayerObject.description
                )
            )
    }

    override fun onCancelDialog(cancelDialog: Unit) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    lateinit var messageArray : Array<String>
//    lateinit var contacts: List<BroadcastData>
    private lateinit var broadcast_fab1: FloatingActionButton
    private lateinit var fab_add_bc: FloatingActionButton
    private lateinit var rootView: View
    private val db by lazy { FirebaseFirestore.getInstance() }
    private lateinit var bcOverviewAdapter: BCAdapter
    private val broadcastsCollectionRef = db.collection("broadcasts")
    private val usersCollectionRef = db.collection("users")
    val currentUser = FirebaseAuth.getInstance()
    private var playerName: String? = null

    //Floating Action Button
    private lateinit var fabOpen: Animation
    private lateinit var fabClose: Animation
    private lateinit var fabRotate: Animation
    private lateinit var fabRotateAntiClockWise: Animation
    private var isOpen = false

    //ListItem
    var listBroadcasts = mutableListOf<BroadcastData>()
    private lateinit var lvBroadcastOverview: ListView
    private var registration: ListenerRegistration? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        rootView = inflater.inflate(R.layout.fragment_webview_broadcast_overview, container, false)

        db.collection("broadcasts")
            .get()
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    listBroadcasts.clear()
                    for (document in task.result!!) {
                        Log.d(ContentValues.TAG, "dapet" + document.id + " => " + document.data)
                        listBroadcasts.add(
                            BroadcastData(
                                document.get("broadcastDocumentId").toString(),
                                document.get("playerName").toString(),
                                document.get("playerId").toString(),
                                document.get("playerBroadcastMsg").toString()
                            )
                        )

                        println(  "HIER" +  document.get("broadcastDocumentId").toString() +
                            document.get("playerName").toString() +
                            document.get("playerId").toString() +
                            document.get("playerBroadcastMsg").toString())
                    }
                    bcOverviewAdapter = BCAdapter(context!!, listBroadcasts)
//                    }
                    lvBroadcastOverview.adapter = bcOverviewAdapter
                } else {
                    Log.w(ContentValues.TAG, "Error getting documents.", task.exception)
                }
            }

        return rootView
    }

    override fun onViewCreated(rootView: View, savedInstanceState: Bundle?) {
        super.onViewCreated(rootView, savedInstanceState)
//        val adapterBroadcast = ArrayAdapter(context, R.layout.lv_broadcast, R.id.bcTextview,messageArray)
        val lvBroadcast : ListView = lv_broadcast
//        lvBroadcast.adapter = adapterBroadcast
//
        lvBroadcastOverview = rootView.findViewById(R.id.lv_broadcast)

        fab_add_bc = rootView.findViewById(R.id.broadcast_fab_add_bc)
        broadcast_fab1 = rootView.findViewById(R.id.broadcast_fab1)

        val adapter = BCAdapter(context!!, listBroadcasts)
        lvBroadcast.adapter = adapter

        initViews(rootView)
        createBroadcastOverviewList()
    }

    private fun createBroadcastOverviewList() {


        bcOverviewAdapter= BCAdapter(context!!, listBroadcasts)
        lvBroadcastOverview.adapter = bcOverviewAdapter

        lvBroadcastOverview.onItemClickListener =
            AdapterView.OnItemClickListener { parent, view, position, id ->
                var playerFromServer =
                    lvBroadcastOverview.getItemAtPosition(position) as BroadcastData

                if(currentUser.uid!!.equals(playerFromServer.playerId)){
                    broadcastsCollectionRef.get().addOnSuccessListener {

                        createPlayerWantsToPostBroadcastDialog(
                            1,
                            GetTimeTool.getCurrentTimestampAsId(),
                            currentUser.uid.toString(),
                            playerName!!,
                            ""
                        )
                    }
                }else{
                    ToastGenerator.message(parent.context, getString(R.string.alert_you_can_only_edit_your_entrys))
                }

            }




        lvBroadcastOverview.setOnItemLongClickListener { parent, view, position, id ->
            var bcFromServer =
                lvBroadcastOverview.getItemAtPosition(position) as BroadcastData

            if(currentUser.uid!!.equals(bcFromServer.playerId)) {

                var playerFromServer =
                    lvBroadcastOverview.getItemAtPosition(position) as BroadcastData
                broadcastsCollectionRef.document(playerFromServer.broadcastID).delete()
                    .addOnSuccessListener { Log.d(ContentValues.TAG, "DocumentSnapshot successfully deleted!") }
                    .addOnFailureListener { e -> Log.w(ContentValues.TAG, "Error deleting document", e) }
            }
            else{
                ToastGenerator.message(parent.context, getString(R.string.alert_you_can_only_delete_your_entrys))
            }
            true
        }
        bcOverviewAdapter.notifyDataSetChanged()
    }

    private fun initViews(view: View) {


        lvBroadcastOverview = view.findViewById(R.id.lv_broadcast)

        fabOpen = AnimationUtils.loadAnimation(context, R.anim.fab_open)
        fabClose = AnimationUtils.loadAnimation(context, R.anim.fab_close)
        fabRotate = AnimationUtils.loadAnimation(context, R.anim.rotate_clockwise)
        fabRotateAntiClockWise = AnimationUtils.loadAnimation(context, R.anim.rotate_anti_clockwise)

        fab_add_bc = rootView.findViewById(R.id.broadcast_fab_add_bc)
        broadcast_fab1 = rootView.findViewById(R.id.broadcast_fab1)

        fab_add_bc.setOnClickListener { view ->
            checkId(view.id)
        }


        broadcast_fab1.setOnClickListener { view ->
            //            checkId(view.id)
            checkId(view.id)

        }
    }

    private fun checkId(view: Int) {
        usersCollectionRef
            .get()
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val currentUserDoc = usersCollectionRef.document(currentUser.uid.toString())
                    currentUserDoc.get().addOnSuccessListener {
                        if (it != null && currentUserDoc.id == currentUser.uid && it.exists()) {
                            playerName = it.get("username").toString()
                        }
                    }
                }
            }


        when (view) {
            R.id.broadcast_fab_add_bc ->
                animateFAB()

            R.id.broadcast_fab1 -> {
                //1 = neu, 0 = edit
                //Möglicher Bug --> playerStatus könnt auf 1 bleiben
                if(playerName.equals(null)){
                    playerName = ""
                }

                createPlayerWantsToPostBroadcastDialog(
                    1,
                    GetTimeTool.getCurrentTimestampAsId(),
                    currentUser.uid.toString(),
                    playerName!!,
                    bcTextview?.text.toString()

                )
                bcOverviewAdapter.notifyDataSetChanged()
            }
        }
    }

    private fun animateFAB() {
        if (isOpen) {
            fab_add_bc.startAnimation(fabRotateAntiClockWise)
            broadcast_fab1.startAnimation(fabClose)
            broadcast_fab1.isClickable = false
            isOpen = false

        } else {
            fab_add_bc.startAnimation(fabRotate)
            broadcast_fab1.startAnimation(fabOpen)
            broadcast_fab1.isClickable = true
            isOpen = true


        }
    }

    override fun onStart() {
        super.onStart()
        broadcastsCollectionRef
            .addSnapshotListener { value, e ->
                if (e != null) {
                    Log.w(ContentValues.TAG, "Listen failed.", e)
                    return@addSnapshotListener
                }
                if (!value!!.isEmpty) {

                    listBroadcasts.clear()

                    for (doc in value) {
                        val broadcastDataObj = doc.toObject(BroadcastData::class.java)
                        listBroadcasts.add(broadcastDataObj)
                    }
                    bcOverviewAdapter.notifyDataSetChanged()
                }
            }
    }

    override fun onStop() {
        super.onStop()
        registration?.remove()
    }

    private fun createPlayerWantsToPostBroadcastDialog(
        addOrUpdate: Int,
        broadcastDocumentId: String,
        playerId: String,
        playerName: String,
        broadcastDescription: String
    ) {
        val ft = fragmentManager!!.beginTransaction()
        when (addOrUpdate) {
            1 -> {
                val newFragment = UserCreatesBroadcastDialogFragment.newInstance(
                )
                newFragment.setTargetFragment(this, 0)
                newFragment.show(ft, "dialog")
            }
            0 -> {
                val newFragment = PlayerWantsToPlayDialogFragment.newInstance(
                )
                val args = Bundle()
                args.putSerializable("broadcastDocumentId", broadcastDocumentId)
                args.putSerializable("playerId", playerId)
                args.putSerializable("playerName", playerName)
                args.putSerializable("broadcastDescription", broadcastDescription)

                newFragment.arguments = args
//                playersCollectionRef.document(playerDocumentId).update()
                newFragment.setTargetFragment(this, 0)
                newFragment.show(ft, "dialog")
            }
        }

    }
}