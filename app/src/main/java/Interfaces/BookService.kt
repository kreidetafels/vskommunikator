//package Interfaces
//
//import com.example.VSKommunikator.Data.Book
//import retrofit2.Call
//import retrofit2.http.Body
//import retrofit2.http.POST
//import retrofit2.http.GET
//import retrofit2.http.Path
//
//
///**
// * @author: Robert Neumann (Matrikel-Nr.: 15589) on 07.07.2019.
// * @params:
// * ToDo:
// * @note:
// */
//interface BookService {
//    @GET("books")
//    fun all(): Call<List<Book>>
//
//    @GET("books/{isbn}")
//    operator fun get(@Path("isbn") isbn: String): Call<Book>
//
//    @POST("books/new")
//    fun create(@Body book: Book): Call<Book>
//}