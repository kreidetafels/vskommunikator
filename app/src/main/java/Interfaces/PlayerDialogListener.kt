package Interfaces

import com.example.VSKommunikator.Data.BroadcastData
import com.example.VSKommunikator.Data.PlayerOverviewData

/**
 * @author: Robert Neumann (Matrikel-Nr.: 15589) on 30.06.2019.
 * @params:
 * @note:
 */

interface PlayerDialogListener {
    fun onSelectDoneDialog(mPlayerObject: PlayerOverviewData)
    fun onCancelDialog(cancelDialog: Unit)
}
