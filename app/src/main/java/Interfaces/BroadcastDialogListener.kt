package Interfaces

import com.example.VSKommunikator.BroadcastOverview
import com.example.VSKommunikator.Data.BroadcastData
import com.example.VSKommunikator.Data.PlayerOverviewData

/**
 * @author: Robert Neumann (Matrikel-Nr.: 15589) on 30.06.2019.
 * @params:
 * @note:
 */

interface BroadcastDialogListener {
    fun onSelectDoneDialog(mPlayerObject: BroadcastData)
    fun onCancelDialog(cancelDialog: Unit)
}
